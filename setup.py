from setuptools import setup, find_packages

setup(
    name='cluster',
    description='Sample package for Python-Guide.org',
    packages=find_packages()
)
