# README #

### What is this repository for? ###

* Python package to submit jobs to the cluster within python. 

### How do I get set up? ###

```
git clone https://oscarlr@bitbucket.org/oscarlr/cluster.git
cd cluster
python setup.py install
```

Edit `cluster/config.py` to set up your default parameters that way you don't have to run Lsf.config to configure the cluster to your liking.
### Examples ###
Function that I normally create that takes a list of bash scripts and submits it into the cluster. (`hpc.submit` can take an arbitrary command).
```
def submit(self,bashfns):
    hpc = Lsf()
    hpc.config(cpu=self.threads,walltime=self.walltime,memory=self.mem,queue=self.queue)
    for bashfn in bashfns:
        hpc.submit("%s" % bashfn)
    hpc.wait()
    status,failed_bashes = hpc.status()
    if 2 in status:
        sys.exit("Job failed in cluster")
```